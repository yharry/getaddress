<?php

/**
 * @author yousaf harry
 * @copyright 2015
 */

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : die('Nothing to do');
// Available actions
$actions = array();
array_push($actions, array('name' => 'getAddress', 'action' => 'getAddress'));
// Go through the actions list and run the associated functions
foreach ($actions as $act) {
    if ($act['name'] == $action) {
        $functionName = $act['action'] . '();';
        eval($functionName);
    }
}

function getAddress(){
    $response = array('success'=> true);

    $postCode = $_POST['postCode'];
    $url = 'https://api.getaddress.io/v2/uk/'.$postCode.'?api-key=bt_nT2D9DUiz1ZB-gKy7cg3219';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_COOKIE, "cookiename=1");
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_POST, 1);

    $out = curl_exec($ch);
    curl_setopt($ch, CURLOPT_HTTPGET, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));

    $html = curl_exec($ch);
    curl_close($ch);

    $response['url'] = $html;
    echo json_encode($response);
}


?>