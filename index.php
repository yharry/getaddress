﻿
<!DOCTYPE html>
<head>

    <title>Get Address</title>
    <link rel="stylesheet" href="bootstrap.css"  type="text/css">
    <script src="jquery.min.js"></script>

    <!-- Include plugin file -->
    <script src="https://getaddress.io/js/jquery.getAddress-2.0.1.min.js"></script>

    <!-- Add after your form -->
    <script type="text/javascript">

        function getResult() {

            $("#loading").css("display", "block");
            var postCode = $('#opc_input').val();

            fields = {postCode : postCode};
            var url = "service.php?action=getAddress";

            $.post(url,fields,function (response) {
                $("#loading").css("display", "none");
                if (response.success) {
                    var res  = response.url;
                    var address = eval('(' + res + ')');
                    showResultSet(address);
                }else {
                    console.log("error");
                }
            },'json');

        }

        function showResultSet(optionList){
            console.log(optionList.Addresses);
            var addressess = optionList.Addresses;

            var id = 'id_';
            var name = 'name_';
            var combo = $("<select class='form-control' onclick='setValues()' multiple></select>").attr("id", id).attr("name", name);

            $.each(addressess, function (i, el) {
                combo.append("<option>" + el + "</option>");
            });

            $("#resultSet").append(combo);
        }

    </script>

    <style type="text/css">
        #opc_button{
            background-color: #b3c833;
            border-color: #b3c833;
            color: #fff;
        }

        #opc_input {
            display: inline;
            margin-right: 5%;
            width: 50%;
        }


    </style>

</head>

<body class="login">

<h3 style="text-align: center">Get Address</h3>
        <form class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 ng-pristine ng-valid" role="form">

            <div class="form-group">
                <div id="postcode_lookup">
                        <input type="text" id="opc_input" class="form-control" autocomplete="off" style="color:#CBCBCB;">
                        <button onclick="getResult()" id="opc_button" type="button" class="btn btn-default btn-teplok">Find address</button>
                </div>
            </div>

            <div class="form-group" id="resultSet">

            </div>

            <div class="form-group" id="loading" style="display: none">
                <img src="loading.gif" alt="Loading" />
            </div>

            <!--<div class="form-group">
                <label class="control-label" for="line1">First Address Line</label>
                <input type="text" id="line1" class="form-control">
            </div>

            <div class="form-group">
                <label class="control-label" for="line1">Second Address Line</label>
                <input type="text" id="line2" class="form-control">
            </div>

            <div class="form-group">
                <label class="control-label" for="line1">Third Address Line</label>
                <input type="text" id="line3" class="form-control">
            </div>

            <div class="form-group">
                <label class="control-label" for="line1">Town</label>
                <input type="text" id="town" class="form-control">
            </div>

            <div class="form-group">
                <label class="control-label" for="line1">County</label>
                <input type="text" id="county" class="form-control">
            </div>

            <div class="form-group">
                <label class="control-label" for="line1">Postcode</label>
                <input type="text" id="postcode-demo" class="form-control">
            </div>-->

        </form>

    </body>

</html>